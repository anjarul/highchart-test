import * as Highcharts from 'highcharts';
import { Component, Input, OnInit} from '@angular/core';
import { AuthService} from '../../../shared/services/auth.service';
import { IStats} from '../../../model/stats';
import { Observable} from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reason-of-unsatisfied-chart-male',
  templateUrl: './reason-of-unsatisfied-chart-male.component.html',
  styleUrls: ['./reason-of-unsatisfied-chart-male.component.css']
})

export class ReasonOfUnsatisfiedChartMaleComponent implements OnInit {
 datas: IStats;
 totalData: any;

  constructor(private auth: AuthService, private router: Router) {}

  @Input() statsDataSubject: Observable<any>;

  ngOnInit() {

    this.datas = null;
    this.statsDataSubject.subscribe(data => {
      this.datas = data;
      this.loadData();
    }, err => {
      if (err.status === 401) {
       localStorage.clear();
       this.router.navigateByUrl('/login');
     } else if (err.status === 0) {
      this.router.navigateByUrl('/login');
      localStorage.clear();
    } else {
       console.log(err);
     }
   });


  }

  loadData() {
    const  chartLevel = {
      0: '০%',
      5: '৫%',
      10: '১০%',
      15: '১৫%',
      20: '২০%',
      25: '২৫%',
      30: '৩০%',
      35: '৩৫%',
      40: '৪০%',
      45: '৪৫%',
      50: '৫০%',
      55: '৫৫%',
      60: '৬০%',
      65: '৬৫%',
      70: '৭০%',
      75: '৭৫%',
      80: '৮০%',
      85: '৮৫%',
      90: '৯০%',
      95: '৯৫%',
      100: '১০০%'
    };
    Highcharts.chart('containerMale', {
      chart: {
        type: 'bar',
      },
      title: {
        text: '<h4>অসন্তুষ্টির কারন সমূহ পুরুষ</h4>',
        style: {
          fontSize: '1.318vw',
          fontFamily: 'Kalpurush,Arial,sans-serif !important',
      }
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        categories: this.getCategory(),
        title: {
          text: null
        },
        labels: {
          // format: '{value}'
          style: {
            fontSize: '0.879vw',
            fontFamily: 'Kalpurush,Arial,sans-serif !important',
          }
        }
      },
      yAxis: {
        min: 0,
        allowDecimals: false,
        title: {
          text: '',
          align: 'high'
        },
        labels: {
          formatter() {
            const value = chartLevel[this.value];
            return value != 'undefined' ? value : this.value;
          },
          // format: '{value}%',
          overflow: 'justify'
        }
      },
      tooltip: {
        // headerFormat: '<span style="font-size: 10px">{point.yCategory}%</span><br/>',
        // pointFormat: '{point.y:.2f}%',
        formatter() {
          let numberInbangla: any = '';
          const eng = this.y.toFixed(1).toString();
          // tslint:disable-next-line: prefer-for-of
          for (let i = 0; i < eng.length; i++) {
            if (eng[i] == '.') {
              numberInbangla += '.';
              continue;
            }
            if ( Number(eng[i]) >= 0 && Number(eng[i]) <= 9) {
              if (eng[i] == '0') { numberInbangla += '০'; }
              else if (eng[i] == '0') { numberInbangla += '০'; }
              else if (eng[i] == '1') { numberInbangla += '১'; }
              else if (eng[i] == '2') { numberInbangla += '২'; }
              else if (eng[i] == '3') { numberInbangla += '৩'; }
              else if (eng[i] == '4') { numberInbangla += '৪'; }
              else if (eng[i] == '5') { numberInbangla += '৫'; }
              else if (eng[i] == '6') { numberInbangla += '৬'; }
              else if (eng[i] == '7') { numberInbangla += '৭'; }
              else if (eng[i] == '8') { numberInbangla += '৮'; }
              else if (eng[i] == '9') { numberInbangla += '৯'; }
            }
          }
          return this.x + '<br/>' + numberInbangla + '%';
        },
        style: {
          fontSize: '0.879vw',
          fontFamily: 'Kalpurush,Arial,sans-serif !important',
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true,
            inside: true
          }
        },
      },
      legend: {
        layout: 'vertical',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
          Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: [{
        type: 'bar',
        name: '',
        data: this.getDissatisfiedReasons(),
        // dataLabels: [{
        //   format: '{point.y:.1f}%'
        // }]
      dataLabels: [{
        enabled: true,
        // format: '{point.y:.1f}%',
        formatter() {
          let numberInbangla: any = '';
          const eng = this.y.toFixed(1).toString();
          // tslint:disable-next-line: prefer-for-of
          for (let i = 0; i < eng.length; i++) {
            if (eng[i] == '.') {
              numberInbangla += '.';
              continue;
            }
            if ( Number(eng[i]) >= 0 && Number(eng[i]) <= 9) {
              if (eng[i] == '0') { numberInbangla += '০'; }
              else if (eng[i] == '0') { numberInbangla += '০'; }
              else if (eng[i] == '1') { numberInbangla += '১'; }
              else if (eng[i] == '2') { numberInbangla += '২'; }
              else if (eng[i] == '3') { numberInbangla += '৩'; }
              else if (eng[i] == '4') { numberInbangla += '৪'; }
              else if (eng[i] == '5') { numberInbangla += '৫'; }
              else if (eng[i] == '6') { numberInbangla += '৬'; }
              else if (eng[i] == '7') { numberInbangla += '৭'; }
              else if (eng[i] == '8') { numberInbangla += '৮'; }
              else if (eng[i] == '9') { numberInbangla += '৯'; }
            }
          }
          return numberInbangla + '%';
        },
        style: {
          fontSize: '0.879vw',
          fontFamily: 'Kalpurush,Arial,sans-serif !important',
        }
      }]
      }]
    });
  }

  getDissatisfiedReasons() {
    let totalCount= 0;
    const dissatisfiedValues: any[] = [];
    if (this.datas && this.datas.dissatisfaction_causes_male) {
      this.datas.dissatisfaction_causes_male.forEach(data => {
        totalCount += data.count;
      });
      this.datas.dissatisfaction_causes_male.forEach(data => {
        dissatisfiedValues.push((data.count * 100) / totalCount);
      });
    }
    this.totalData = totalCount;
    return dissatisfiedValues;
  }

  dataInNumber() {
  const num: any[] = [];
  if (this.datas) {
    this.datas.dissatisfaction_causes_male.forEach(data => {
      num.push(data.count);
    });
    return num;
  }
}

  getCategory() {
    const categories: any[] = [];
    if (this.datas && this.datas.dissatisfaction_causes_male) {
      this.datas.dissatisfaction_causes_male.forEach(data => {
        categories.push(data.reason);
      });
    }
    return categories;
  }
}
